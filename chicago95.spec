# Reference:
#    https://src.fedoraproject.org/rpms/nagios-plugins/blob/master/f/nagios-plugins.spec
#    debian/ for this package
# Documentation:
#    This file is manually copied to the OBS project chicago95 due to difficulties with the _service operation.
%define  debug_package %{nil}
%define  committish master
%define archivename %{committish}.tar.gz
%define archivedir Chicago95-%{committish}
%if 0%{?obsbuild}
%define archivename chicago95.orig.tar.gz
%define archivedir chicago95.orig
%endif
# for OBS, Fedora 30, 31
%if "0%{?_fontbasedir}" == "0"
%define _fontbasedir %{_datadir}/fonts
%endif

Name:       chicago95-theme
Version:    2.0.1
Release:    1%{?dist}
Summary:    XFCE Windows 95 Total Conversion

Group:      User Interface/Desktops
License:    GPLv3.0+/MIT
URL:        https://github.com/grassmunk/Chicago95
Source0:    https://github.com/grassmunk/Chicago95/archive/%{archivename}
Patch0:     paths.patch

BuildArch:  noarch
Packager:   B Stack <bgstack15@gmail.com>
BuildRequires: txt2man

%description
Assets to allow customization of Xfce to look as
close to Microsoft Windows 95 as possible. This
metapackage depends on most of the other chicago95
packages.

%package all
Summary: Chicago95 theme - all components
Requires: %{name}-backgrounds
Requires: %{name}-cursors
Requires: %{name}-doc
Requires: %{name}-fonts
Requires: %{name}-gtk
Requires: %{name}-icons
Requires: %{name}-login-sound
Requires: %{name}-plus
Requires: %{name}-plymouth
Requires: %{name}-sounds
%description all
Assets to allow customization of Xfce to look as
close to Microsoft Windows 95 as possible. This
metapackage depends on most of the other chicago95
packages.

%package backgrounds
Summary: Backgrounds for Chicago95
%description backgrounds
Backgrounds that resemble some of the classic Windows 95
wallpapers. This is part of the chicago95 theme suite.

%package cursors
Summary: Mouse cursor themes for Chicago95
%description cursors
Mouse cursors for the Chicago95 theme suite.

%package doc
Summary: Documentation for Chicago95
%description doc
Documentation and readmes for the Chicago95 theme
suite, including ultimate ricing guides.

%package fonts
Summary: Fonts for Chicago95
%description fonts
Terminal fonts for the Chicago95 theme suite.
Overall system fonts are proprietary but described
in the documentation for the theme.

%package gtk
%if !0%{?el7}
Recommends: qt5-qtstyleplugins
%endif
Conflicts: %{name}-greeter < %{version}
Summary: GTK and WM themes for Chicago95
%description gtk
Themes for GTK2, GTK3, Metacity, and more, for the
Chicago95 theme suite.

%package icons
Summary: Icon themes for Chicago95
%description icons
Icon themes for Chicago95 theme suite.

%package plymouth
Requires: plymouth
Summary: Plymouth theme for Chicago95
%description plymouth
Graphical theme for Plymouth bootloader, for the
Chicago95 theme suite.

%package sounds
Summary: Sounds for Chicago95
%description sounds
UI sounds for Chicago95 theme.

%package login-sound
Requires: sox
Summary: Login sound for Chicago95
%description login-sound
XDG autostart and sound file for Chicago95 theme suite.

%package plus
Requires: inkscape
Requires: python3
Requires: python3-fonttools
Requires: python3-numpy
Requires: python3-svgwrite
Summary: Windows Plus! Theme Conversion for XFCE/Chicago95
%description plus
Translate Windows 95 Plus! themes to Gtk-compatible themes.

%prep
%setup -n %{archivedir}

%patch0 -p1

%build
%make_build

%install
export use_underscores=YES
%make_install

%clean
%{__rm} -rf %{buildroot} || :

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%preun
# is it a final removal?
#if test "$1" = "0" ;
#then
#fi   

%postun
if test "$1" = "0" ;
then
   touch --no-create %{_datadir}/icons/hicolor &>/dev/null
fi

%posttrans plus
update-desktop-database 1>/dev/null 2>&1 & :
gtk-update-icon-cache %{_datadir}/icons/hicolor 1>/dev/null 2>&1 & :
update-mime-database -n ${_datadir}/mime 1>/dev/null 2>&1 & :

%files all

%files backgrounds
%{_datadir}/backgrounds/Chicago95

%files cursors
%{_datadir}/icons/Chicago95_Cursor_Black/cursors
%{_datadir}/icons/Chicago95_Cursor_Black/index.theme
%{_datadir}/icons/Chicago95_Cursor_White/cursors
%{_datadir}/icons/Chicago95_Cursor_White/index.theme
%{_datadir}/icons/Chicago95_Emerald/cursors
%{_datadir}/icons/Chicago95_Emerald/index.theme
%{_datadir}/icons/Chicago95_Animated_Hourglass_Cursors/cursors
%{_datadir}/icons/Chicago95_Animated_Hourglass_Cursors/index.theme
%{_datadir}/icons/Chicago95_Standard_Cursors/cursors
%{_datadir}/icons/Chicago95_Standard_Cursors/index.theme
%{_datadir}/icons/Chicago95_Standard_Cursors_Black/cursors
%{_datadir}/icons/Chicago95_Standard_Cursors_Black/index.theme

%files doc
%{_docdir}/chicago95

%files fonts
%{_fontbasedir}/truetype/*

%files gtk
%{_datadir}/xfce4/terminal/colorschemes/Chicago95.theme
%{_datadir}/themes/Chicago95
%{_datadir}/xfce4-panel-profiles/layouts/Chicago95*

%files icons
%{_datadir}/icons/Chicago95
%{_datadir}/icons/Chicago95-tux

%files login-sound
%{_datadir}/sounds/Chicago95/startup.ogg
%{_sysconfdir}/xdg/autostart/chicago95-startup.desktop

%files plus
%{_datadir}/icons/Chicago95_Cursor_Black/src
%{_datadir}/icons/Chicago95_Cursor_White/src
%{_datadir}/icons/Chicago95_Emerald/src
%{_datadir}/icons/Chicago95_Animated_Hourglass_Cursors/build
%{_datadir}/icons/Chicago95_Standard_Cursors/build
%{_datadir}/icons/Chicago95_Standard_Cursors_Black/build
%{_bindir}/ChicagoPlus
%{_bindir}/PlusGUI
%{_libexecdir}/chicago95-theme-plus
%{_datadir}/applications/PlusGUI.desktop
%{_datadir}/chicago95-theme-plus
%{_datadir}/mime/packages/*.xml
%{_mandir}/man1/ChicagoPlus.1.gz
%{_mandir}/man1/PlusGUI.1.gz

%files plymouth
%{_datadir}/plymouth/themes/*

%files sounds
%{_datadir}/sounds/Chicago95/stereo
%{_datadir}/sounds/Chicago95/index.theme

%changelog
* Tue Jul 14 2020 B Stack <bgstack15@gmail.com> - 2.0.1-1
- Initial rpm release
