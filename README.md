# Readme for chicago95-packages
This is the workplace for the official release packages of [Chicago95](https://github.com/grassmunk/Chicago95) project. Chicago95 is a Windows 95 Total Conversion theme for Xfce, a mid-weight desktop environment for Unix-like operating systems. Xubuntu uses Xfce, and is the primary target, but other distros and systems can use the theme as well.

Explore the package build instructions in this repository to learn how the packages are built. You can either build packages for yourself this way, or use the [official ones](https://software.opensuse.org//download.html?project=home%3Abgstack15%3AChicago95&package=chicago95-theme-all)!
