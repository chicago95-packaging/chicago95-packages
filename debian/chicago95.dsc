Format: 3.0 (quilt)
Source: chicago95
Binary: chicago95-theme-all, chicago95-theme-cursors, chicago95-theme-doc, chicago95-theme-fonts, chicago95-theme-gtk, chicago95-theme-icons, chicago95-theme-plymouth, chicago95-theme-sounds, chicago95-theme-login-sound, chicago95-theme-plus, chicago95-theme-backgrounds
Architecture: all
Version: 2.0.1-1
Maintainer: B Stack <bgstack15@gmail.com>
Homepage: https://github.com/grassmunk/Chicago95
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 9~), txt2man
Package-List:
 chicago95-theme-all deb misc optional arch=all
 chicago95-theme-backgrounds deb misc optional arch=all
 chicago95-theme-cursors deb misc optional arch=all
 chicago95-theme-doc deb misc optional arch=all
 chicago95-theme-fonts deb misc optional arch=all
 chicago95-theme-gtk deb misc optional arch=all
 chicago95-theme-icons deb misc optional arch=all
 chicago95-theme-login-sound deb misc optional arch=all
 chicago95-theme-plus deb misc optional arch=all
 chicago95-theme-plymouth deb misc optional arch=all
 chicago95-theme-sounds deb misc optional arch=all
Files:
 00000000000000000000000000000000 0 chicago95.orig.tar.gz
 00000000000000000000000000000000 0 chicago95.debian.tar.xz

